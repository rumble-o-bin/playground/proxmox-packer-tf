#!/bin/bash
set -euxo pipefail

# configure apt for non-interactive mode.
export DEBIAN_FRONTEND=noninteractive

# switch to the non-enterprise repository.
# see https://pve.proxmox.com/wiki/Package_Repositories
rm -f /etc/apt/sources.list.d/pve-enterprise.list
echo "deb http://download.proxmox.com/debian/pve $(. /etc/os-release && echo "$VERSION_CODENAME") pve-no-subscription" >/etc/apt/sources.list.d/pve.list

# # switch the apt mirror from us to nl.
sed -i -E 's,ftp\.us\.debian,ftp.sg.debian,' /etc/apt/sources.list
# sed -i -E 's,http:\/\/(deb|security)\.debian\.org,http:\/\/ftp.sg.debian.org,' /etc/apt/sources.list

# upgrade.
apt update --fix-missing
# apt upgrade -y