variable "cpus" {
  type    = string
  default = "2"
}

variable "disk_size" {
  type    = string
  default = "20480"
}

variable "iso_checksum" {
  type    = string
  default = "sha256:55b672c4b0d2bdcbff9910eea43df3b269aaab3f23e7a1df18b82d92eb995916"
}

variable "iso_url" {
  type    = string
  default = "iso/proxmox-ve_7.4-1.iso"
}

variable "memory" {
  type    = string
  default = "2048"
}

variable "output_base_dir" {
  type    = string
  default = "dist/"
}

variable "shell_provisioner_scripts" {
  type    = list(string)
  default = ["provisioners/upgrade.sh", "provisioners/network.sh", "provisioners/reboot.sh", "provisioners/provision.sh"]
}

variable "step_country" {
  type    = string
  default = "United S<wait>t<wait>a<wait>t<wait>e<wait>s<wait><enter><wait>"
}

variable "step_email" {
  type    = string
  default = "pve@example.com"
}

variable "step_hostname" {
  type    = string
  default = "pve.example.com"
}

variable "step_keyboard_layout" {
  type    = string
  default = ""
}

variable "step_timezone" {
  type    = string
  default = ""
}

source "virtualbox-iso" "proxmox-ve-amd64-virtualbox" {
  boot_command         = ["<enter>", "<wait1m>", "<enter><wait>", "<enter><wait>", "${var.step_country}<tab><wait>", "${var.step_timezone}<tab><wait>", "${var.step_keyboard_layout}<tab><wait>", "<tab><wait>", "<enter><wait5>", "vagrant<tab><wait>", "vagrant<tab><wait>", "${var.step_email}<tab><wait>", "<tab><wait>", "<enter><wait5>", "${var.step_hostname}<tab><wait>", "<tab><wait>", "<tab><wait>", "<tab><wait>", "<tab><wait>", "<tab><wait>", "<enter><wait5>", "<enter><wait5>"]
  boot_wait            = "5s"
  disk_size            = "${var.disk_size}"
  guest_additions_mode = "attach"
  guest_os_type        = "Debian_64"
  hard_drive_discard   = true
  hard_drive_interface = "sata"
  headless             = true
  iso_checksum         = "${var.iso_checksum}"
  iso_url              = "${var.iso_url}"
  output_directory     = "${var.output_base_dir}"
  shutdown_command     = "poweroff"
  ssh_password         = "vagrant"
  ssh_timeout          = "60m"
  ssh_username         = "root"
  vboxmanage           = [["modifyvm", "{{ .Name }}", "--memory", "${var.memory}"], ["modifyvm", "{{ .Name }}", "--cpus", "${var.cpus}"], ["modifyvm", "{{ .Name }}", "--nested-hw-virt", "on"], ["modifyvm", "{{ .Name }}", "--vram", "16"], ["modifyvm", "{{ .Name }}", "--graphicscontroller", "vmsvga"], ["modifyvm", "{{ .Name }}", "--audio", "none"], ["modifyvm", "{{ .Name }}", "--nictype1", "82540EM"], ["modifyvm", "{{ .Name }}", "--nictype2", "82540EM"], ["modifyvm", "{{ .Name }}", "--nictype3", "82540EM"], ["modifyvm", "{{ .Name }}", "--nictype4", "82540EM"]]
  vboxmanage_post      = [["storagectl", "{{ .Name }}", "--name", "IDE Controller", "--remove"]]
}

build {
  sources = ["source.virtualbox-iso.proxmox-ve-amd64-virtualbox"]

  provisioner "shell" {
    expect_disconnect = true
    scripts           = var.shell_provisioner_scripts
  }

  post-processors {
    post-processor "vagrant" {
      keep_input_artifact = true
      provider_override   = "virtualbox"
      output = "${var.output_base_dir}/proxmox-ve-amd64-virtualbox.box"
    }
  }
}
