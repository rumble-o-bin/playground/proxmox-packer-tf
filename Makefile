# Variables
PACKER_FILES="proxmox-ve.json.pkr.hcl"
VAGRANT_BOX="proxmox-ve-amd64-virtualbox.box"

.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

build: ## Build packer
	@packer build -timestamp-ui -force $(PACKER_FILES)

box: ## Add vagrant box
	@vagrant box add --name proxmox -f dist/$(VAGRANT_BOX)