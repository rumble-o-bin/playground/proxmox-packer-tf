# proxmox-packer-tf

> TBH main objective this repository was want to play terraform with proxmox.
> That's why need proxmox running top of `vagrant` as development environment
> Shout big thanks and highly inspired from: https://github.com/rgl/proxmox-ve

## Table of Content

[[_TOC_]]

## Prerequisite

- virtualbox with guest addition
- packer
- vagrant

## Steps

### Create the `.box`

```bash
make build
```

### Add the box 

```bash
make box
```

### Spawning proxmox

```bash
vagrant up
```

## Notes

- The default port for web ui: *:8086*
- Login with
  - user: root
  - password: vagrant

## License

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```